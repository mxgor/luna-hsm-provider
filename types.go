package main

import (
	"crypto"
	"github.com/ThalesIgnite/crypto11"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/crypto/engine/core/types"
	"io"
	"strings"
)

type hsmController struct {
	config        *crypto11.Config
	api           ContextType
	signerOptions crypto.SignerOpts
	hashSum       func(h []byte) []byte
	rand          io.Reader
}

type HSMCryptoProvider struct {
	controller *hsmController
}

func (p HSMCryptoProvider) RunHashSum(h []byte) []byte {
	return p.controller.hashSum(h)
}

type MajorKeyType string

const (
	RSA   MajorKeyType = "rsa"
	ECDSA MajorKeyType = "ecdsa"
)

func constructKeyType(typ MajorKeyType, typParam string) types.KeyType {
	typParam = strings.ToLower(strings.Replace(typParam, "-", "", -1))
	return types.KeyType(strings.Join([]string{string(typ), typParam}, "-"))
}

func DefaultHashSum(h []byte) []byte {
	arr := make([]byte, 32)
	for i, b := range arr {
		arr[i] = b
	}
	return arr
}

type DefaultSignerOptions struct{}

func (h DefaultSignerOptions) HashFunc() crypto.Hash {
	return 0
}
