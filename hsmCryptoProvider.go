package main

import (
	"context"
	"crypto"
	"crypto/ecdsa"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	b64 "encoding/base64"
	"errors"
	"fmt"
	"github.com/ThalesIgnite/crypto11"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/crypto/engine/core/types"
	"math/rand"
	"strconv"
)

const (
	HsmNamespace = "luna-cloud-hsm"
)

func getNotFoundError(keyId string) error {
	return fmt.Errorf("could not find key with identifier `%s`", keyId)
}

func (p HSMCryptoProvider) getSigner(parameter types.CryptoIdentifier) (crypto11.Signer, error) {
	id := []byte(parameter.KeyId)
	signer, err := p.controller.api.FindKeyPair(id, nil)
	if signer == nil && err == nil {
		return nil, getNotFoundError(parameter.KeyId)
	}
	return signer, err
}

func (p HSMCryptoProvider) GetNamespaces(context types.CryptoContext) ([]string, error) {
	return []string{HsmNamespace}, nil
}
func (p HSMCryptoProvider) GenerateRandom(context types.CryptoContext, number int) ([]byte, error) {
	key := make([]byte, number)
	reader, err := p.controller.api.NewRandomReader()
	if err != nil {
		return nil, err
	}
	_, err = reader.Read(key)
	if err != nil {
		return nil, err
	}
	return key, nil
}
func (p HSMCryptoProvider) Hash(parameter types.CryptoHashParameter, msg []byte) ([]byte, error) {
	// todo use hsm?
	if parameter.HashAlgorithm == types.Sha2256 {
		msgHash := sha256.New()
		_, err := msgHash.Write(msg)
		if err != nil {
			return nil, err
		}
		msgHashSum := msgHash.Sum(nil)
		return msgHashSum, nil
	} else {
		return nil, errors.ErrUnsupported
	}

}
func (p HSMCryptoProvider) Encrypt(parameter types.CryptoIdentifier, data []byte) ([]byte, error) {
	id := []byte(parameter.KeyId)
	key, err := p.controller.api.FindKey(id, nil)
	if err != nil {
		return nil, err
	}
	if key == nil {
		return nil, getNotFoundError(parameter.KeyId)
	}
	data = padBytes(data, key.BlockSize())
	var res = make([]byte, len(data))
	iv := make([]byte, key.BlockSize())
	mod, err := key.NewCBCEncrypter(iv)
	if err != nil {
		return nil, err
	}
	mod.CryptBlocks(res, data)
	return res, nil
}
func (p HSMCryptoProvider) Decrypt(parameter types.CryptoIdentifier, data []byte) ([]byte, error) {
	id := []byte(parameter.KeyId)
	key, err := p.controller.api.FindKey(id, nil)
	if err != nil {
		return nil, err
	}
	if key == nil {
		return nil, getNotFoundError(parameter.KeyId)
	}

	iv := make([]byte, key.BlockSize())
	mod, err := key.NewCBCDecrypter(iv)
	if err != nil {
		return nil, err
	}
	mod.CryptBlocks(data, data)
	return unpadBytes(data)
}
func (p HSMCryptoProvider) Sign(parameter types.CryptoIdentifier, data []byte) ([]byte, error) {
	signer, err := p.getSigner(parameter)
	if err != nil {
		return nil, err
	}
	hash := p.controller.hashSum(data)
	return signer.Sign(p.controller.rand, hash, p.controller.signerOptions)
}
func (p HSMCryptoProvider) GetKeys(parameter types.CryptoFilter) (*types.CryptoKeySet, error) {
	label := []byte(parameter.Filter.String())
	keyPairs, err := p.controller.api.FindKeyPairs(nil, label)
	if err != nil {
		return nil, err
	} else if keyPairs == nil {
		return nil, nil
	} else {
		keys := make([]types.CryptoKey, len(keyPairs))
		for i, keyPair := range keyPairs {
			id := types.CryptoIdentifier{CryptoContext: parameter.CryptoContext, KeyId: parameter.Filter.String()}
			key, err := mapPublicKeyToCryptoKey(id, keyPair)
			if err != nil {
				return nil, err
			}
			keys[i] = *key
		}
		keySet := &types.CryptoKeySet{Keys: keys}
		return keySet, nil
	}
}
func (p HSMCryptoProvider) GetKey(parameter types.CryptoIdentifier) (*types.CryptoKey, error) {
	signer, err := p.getSigner(parameter)
	if err != nil {
		return nil, err
	}
	pubKeyObj := signer.Public()

	return mapPublicKeyToCryptoKey(parameter, pubKeyObj)
}

func mapPublicKeyToCryptoKey(parameter types.CryptoIdentifier, pubKeyObj crypto.PublicKey) (*types.CryptoKey, error) {
	var key = new(types.CryptoKey)
	var params = new(types.CryptoKeyParameter)
	if pubKey, ok := pubKeyObj.(*ecdsa.PublicKey); ok {
		repr, err := pubKey.ECDH()
		if err != nil {
			return nil, err
		}
		key.Key = repr.Bytes()
		params.Identifier = parameter
		params.KeyType = constructKeyType(ECDSA, pubKey.Curve.Params().Name)
	} else if pubKey, ok := pubKeyObj.(*rsa.PublicKey); ok {
		keyBytes, err := x509.MarshalPKIXPublicKey(pubKey)
		if err != nil {
			return nil, err
		}
		key.Key = keyBytes
		key.Identifier = parameter
		params.KeyType = constructKeyType(RSA, strconv.Itoa(pubKey.Size()))

	} else if pubKey, ok := pubKeyObj.(*crypto11.SecretKey); ok {
		return nil, fmt.Errorf("keys of type %T are not retrievable", pubKey)

	} else {
		return nil, fmt.Errorf("key %s has unsupported key format", parameter.KeyId)
	}

	key.CryptoKeyParameter = *params
	return key, nil
}
func (p HSMCryptoProvider) Verify(parameter types.CryptoIdentifier, data []byte, signature []byte) (bool, error) {
	signer, err := p.getSigner(parameter)
	if err != nil {
		return false, err
	}
	pubKeyObj := signer.Public()
	if pubKey, ok := pubKeyObj.(*ecdsa.PublicKey); ok {
		hashed := p.controller.hashSum(data)
		result := ecdsa.VerifyASN1(pubKey, hashed[:], signature)
		return result, nil
	} else if pubKey, ok := pubKeyObj.(*rsa.PublicKey); ok {
		hashed := p.controller.hashSum(data)
		err = rsa.VerifyPSS(pubKey, p.controller.signerOptions.HashFunc(), hashed[:], signature, nil)
		return err == nil, err
	} else if pubKey, ok := pubKeyObj.(*crypto11.SecretKey); ok {
		return false, fmt.Errorf("keys of type %T are not retrievable", pubKey)
	} else {
		return false, fmt.Errorf("key %s has unsupported key format", parameter.KeyId)
	}
}
func (p HSMCryptoProvider) GenerateKey(parameter types.CryptoKeyParameter) error {
	switch parameter.KeyType {
	case types.Rsa2048, types.Rsa3072, types.Rsa4096:
		_, err := p.generateRSA(parameter)
		return err
	case types.Ecdsap256, types.Ecdsap384, types.Ecdsap521:
		_, err := p.generateECDSA(parameter)
		return err
	case types.Aes256GCM:
		_, err := p.generateAES(parameter)
		return err
	case types.Ed25519:
		_, err := p.generateEDDSA(parameter)
		return err
	default:
		return fmt.Errorf("unsupported key type %v", parameter.KeyType)
	}
}
func (p HSMCryptoProvider) GetSeed(context context.Context) string {
	n := rand.Int()
	random, err := p.GenerateRandom(types.CryptoContext{}, n)
	if err != nil {
		fmt.Print(err.Error())
		return ""
	}
	return b64.StdEncoding.EncodeToString(random)
}

func (p HSMCryptoProvider) CreateCryptoContext(context types.CryptoContext) error {
	return nil
}
func (p HSMCryptoProvider) DestroyCryptoContext(context types.CryptoContext) error {
	return nil
}
func (p HSMCryptoProvider) IsCryptoContextExisting(context types.CryptoContext) (bool, error) {
	return true, nil
}

func (p HSMCryptoProvider) IsKeyExisting(parameter types.CryptoIdentifier) (bool, error) {
	id := []byte(parameter.KeyId)
	keyPair, _ := p.controller.api.FindKeyPair(id, nil)
	if keyPair != nil {
		return true, nil
	}
	secret, err := p.controller.api.FindKey(id, nil)
	if secret != nil {
		return true, nil
	}
	return false, err
}
func (p HSMCryptoProvider) DeleteKey(parameter types.CryptoIdentifier) error {
	id := []byte(parameter.KeyId)
	keyPair, _ := p.controller.api.FindKeyPair(id, nil)
	if keyPair != nil {
		return keyPair.Delete()
	}
	secret, err := p.controller.api.FindKey(id, nil)
	if secret != nil {
		return secret.Delete()
	}
	return err
}
func (p HSMCryptoProvider) RotateKey(parameter types.CryptoIdentifier) error {
	key, err := p.GetKey(parameter)
	if key != nil {
		keyType := key.KeyType
		err = p.DeleteKey(parameter)
		if err == nil {
			return p.GenerateKey(types.CryptoKeyParameter{Identifier: parameter, KeyType: keyType})
		} else {
			return err
		}
	}
	secret, err := p.controller.api.FindKey([]byte(parameter.KeyId), nil)
	if secret != nil {
		keyType := types.Aes256GCM
		err = secret.Delete()
		if err == nil {
			return p.GenerateKey(types.CryptoKeyParameter{Identifier: parameter, KeyType: keyType})
		} else {
			return err
		}
	}
	return err
}
