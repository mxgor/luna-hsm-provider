package main

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"github.com/ThalesIgnite/crypto11"
	"github.com/stretchr/testify/assert"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/crypto/engine/core/types"
	"testing"
)

var testId = "test id"

func getTestHSMCryptoProvider(mock *ContextTypeMock) HSMCryptoProvider {
	return HSMCryptoProvider{
		controller: &hsmController{
			config:        &crypto11.Config{},
			rand:          rand.Reader,
			api:           mock,
			signerOptions: DefaultSignerOptions{},
			hashSum:       DefaultHashSum,
		},
	}
}

func TestHSMCryptoProvider_GenerateKey(t *testing.T) {
	var mockApi = new(ContextTypeMock)
	provider := getTestHSMCryptoProvider(mockApi)
	param := types.CryptoKeyParameter{KeyType: types.Ecdsap256, Identifier: types.CryptoIdentifier{KeyId: testId}}
	mockApi.On("GenerateECDSAKeyPair", []byte(testId), elliptic.P256()).Return(&SignerMock{}, nil)
	_ = provider.GenerateKey(param)
	mockApi.AssertExpectations(t)
}

func TestHSMCryptoProvider_GetKey(t *testing.T) {
	key, _ := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	expected, _ := key.Public().(*ecdsa.PublicKey).ECDH()
	var mockApi = new(ContextTypeMock)
	mockApi.On("FindKeyPair", []byte(testId), []byte(nil)).Return(&SignerMock{private: key, hashSum: DefaultHashSum}, nil)
	provider := getTestHSMCryptoProvider(mockApi)
	actual, _ := provider.GetKey(types.CryptoIdentifier{KeyId: testId})
	assert.Equal(t, expected.Bytes(), actual.Key)
	assert.Equal(t, types.Ecdsap256, actual.CryptoKeyParameter.KeyType)
}

func TestHSMCryptoProvider_Verify(t *testing.T) {
	key, _ := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	var mockApi = new(ContextTypeMock)
	mockApi.On("FindKeyPair", []byte(testId), []byte(nil)).Return(&SignerMock{private: key, hashSum: DefaultHashSum}, nil)
	provider := getTestHSMCryptoProvider(mockApi)
	data := []byte("test data")
	sign, err := provider.Sign(types.CryptoIdentifier{KeyId: testId}, data)
	if err != nil {
		print(err.Error())
	}
	actual, err := provider.Verify(types.CryptoIdentifier{KeyId: testId}, data, sign)
	if err != nil {
		print(err.Error())
	}
	assert.True(t, actual)
}

//type pkcs11Object struct {
//	handle  pkcs11.ObjectHandle
//	context *ContextTypeMock
//}
//
//func TestHSMCryptoProvider_Encrypt_Decrypt(t *testing.T) {
//	var mockApi = new(ContextTypeMock)
//	secret := &crypto11.SecretKey{Cipher: crypto11.CipherAES}
//	mockApi.On("FindKey", []byte(testId), []byte(nil)).Return(secret, nil)
//	provider := getTestHSMCryptoProvider(mockApi)
//	data := []byte("test data")
//	res, err := provider.Encrypt(types.CryptoIdentifier{KeyId: testId}, data)
//	if err != nil {
//		print(err.Error())
//	}
//	print(res)
//	assert.True(t, len(res) > 0)
//}
