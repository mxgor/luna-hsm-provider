package main

import (
	"crypto"
	"crypto/sha256"
	"github.com/ThalesIgnite/crypto11"
	"github.com/spf13/viper"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/crypto/engine/core/types"
)

var Plugin plugin //export Plugin Symbol, dont

type plugin struct{}

func (p *plugin) GetCryptoProvider() types.CryptoProvider {
	viper.AutomaticEnv()
	config := crypto11.Config{
		Path:       viper.GetString("CRYPTO_EXECUTABLE_PATH"),
		TokenLabel: viper.GetString("HSM_PARTITION_LABEL"),
		Pin:        viper.GetString("HSM_PARTITION_PASSWORD"),
	}
	def := hsmController{config: &config, signerOptions: crypto.SHA256, hashSum: func(h []byte) []byte {
		res := sha256.Sum256(h)
		return res[:]
	}}
	//def := hsmController{config: &config, signerOptions: crypto.SHA35, hashSum: DefaultHashSum}

	controller, err := def.withApiAndRandomReader()
	if err != nil {
		panic(err)
	}
	provider := HSMCryptoProvider{controller: controller}
	return provider
}
